package dechmods.dechtech.tileentities;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntityFurnace;

public class TileEntityGenerator extends TileEntityElectricInvBase
{
    public ItemStack fuelSlot;
    public int lastMaxFuelLevel = 1600;
    public int currentFuelLevel;
    public byte overflowBuffer;
    public static final int[] slots = new int[] { 0 };
    
    @Override
    public void updateEntity()
    {
        if (worldObj.isRemote) return;
        
        boolean mayUseMoreFuel = true;
        
        if (chargeLevel < 160000 && fuelSlot != null && currentFuelLevel == 0)
        {
            currentFuelLevel = lastMaxFuelLevel = TileEntityFurnace.getItemBurnTime(fuelSlot);
            decrStackSize(0, 1);
        }
        
        if (overflowBuffer > 0 && chargeLevel < 160000)
        {
            if (160000 - chargeLevel <= overflowBuffer)
            {
                chargeLevel += overflowBuffer;
                overflowBuffer = 0;
            }
            else
            {
                overflowBuffer -= (160000 - chargeLevel);
                chargeLevel = 160000;
            }
        }
        
        if (currentFuelLevel >= 4 && chargeLevel <= 159960)
        {
            currentFuelLevel -= 4;
            chargeLevel += 40;
            mayUseMoreFuel = false;
        }
        else if (currentFuelLevel > 0 && currentFuelLevel < 4 && chargeLevel <= 160000 - (currentFuelLevel * 10))
        {   
            chargeLevel += (currentFuelLevel * 10);
            currentFuelLevel = 0;
            mayUseMoreFuel = false;
        }
        else if (currentFuelLevel > 0 && chargeLevel <= 159990)
        {
            int delta = 1;
            if (currentFuelLevel > 1 && chargeLevel <= 159980) delta++;
            if (currentFuelLevel > 2 && chargeLevel <= 159970) delta++;
            
            chargeLevel += delta * 10;
            currentFuelLevel -= delta;            
        }
        
        if (mayUseMoreFuel && currentFuelLevel > 0 && chargeLevel > 159990 && chargeLevel < 160000)
        {
            overflowBuffer = (byte)(chargeLevel - 159990);
            chargeLevel = 160000;
        }
    }
    
    @Override
    public int getSizeInventory()
    {
        return 1;
    }

    @Override
    public ItemStack getStackInSlot(int slotID)
    {
        return fuelSlot;
    }

    @Override
    public void setInventorySlotContents(int slotID, ItemStack stack)
    {
        fuelSlot = stack;
    }

    @Override
    public String getInvName()
    {
        return "Electric Generator";
    }
    
    @Override
    public boolean isItemValidForSlot(int slotID, ItemStack stack)
    {
        return TileEntityFurnace.isItemFuel(stack);
    }

    @Override
    public int[] getAccessibleSlotsFromSide(int side)
    {
        return slots;
    }

    @Override
    public boolean canInsertItem(int slot, ItemStack stack, int side)
    {
        return TileEntityFurnace.isItemFuel(stack);
    }

    @Override
    public boolean canExtractItem(int slot, ItemStack stack, int side)
    {
        return false;
    }
    
    @Override
    public void readFromNBT(NBTTagCompound compound)
    {
        super.readFromNBT(compound);
        
        lastMaxFuelLevel = compound.getInteger("lastMaxFuelLevel");
        currentFuelLevel = compound.getInteger("currentFuelLevel");
        overflowBuffer = compound.getByte("overflowBuffer");
    }
    
    @Override
    public void writeToNBT(NBTTagCompound compound)
    {
        super.writeToNBT(compound);
        
        compound.setInteger("lastMaxFuelLevel", lastMaxFuelLevel);
        compound.setInteger("currentFuelLevel", currentFuelLevel);
        compound.setByte("overflowBuffer", overflowBuffer);
    }

    @Override
    public String getMachineType()
    {
        return "EMITTER";
    }
}

/*package dechmods.dechtech.tileentities;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityFurnace;

public class TileEntityGenerator extends TileEntity implements IInventory
{
    public int chargeLevel;
    public ItemStack fuelStack = null;
    public int lastMaxFuelLevel = 1600;
    public int currentFuelLevel;
    public byte overflowBuffer;
    
    @Override
    public void updateEntity()
    {
        if (worldObj.isRemote) return;
        
        boolean mayUseMoreFuel = true;
        
        if (chargeLevel < 160000 && fuelStack != null && currentFuelLevel == 0)
        {
            currentFuelLevel = lastMaxFuelLevel = TileEntityFurnace.getItemBurnTime(fuelStack);
            decrStackSize(0, 1);
        }
        
        if (overflowBuffer > 0 && chargeLevel < 160000)
        {
            if (160000 - chargeLevel <= overflowBuffer)
            {
                chargeLevel += overflowBuffer;
                overflowBuffer = 0;
            }
            else
            {
                overflowBuffer -= (160000 - chargeLevel);
                chargeLevel = 160000;
            }
        }
        
        if (currentFuelLevel >= 4 && chargeLevel <= 159960)
        {
            currentFuelLevel -= 4;
            chargeLevel += 40;
            mayUseMoreFuel = false;
        }
        else if (currentFuelLevel > 0 && currentFuelLevel < 4 && chargeLevel <= 160000 - (currentFuelLevel * 10))
        {   
            chargeLevel += (currentFuelLevel * 10);
            currentFuelLevel = 0;
            mayUseMoreFuel = false;
        }
        else if (currentFuelLevel > 0 && chargeLevel <= 159990)
        {
            int delta = 1;
            if (currentFuelLevel > 1 && chargeLevel <= 159980) delta++;
            if (currentFuelLevel > 2 && chargeLevel <= 159970) delta++;
            
            chargeLevel += delta * 10;
            currentFuelLevel -= delta;            
        }
        
        if (mayUseMoreFuel && currentFuelLevel > 0 && chargeLevel > 159990 && chargeLevel < 160000)
        {
            overflowBuffer = (byte)(chargeLevel - 159990);
            chargeLevel = 160000;
        }
    }
    
    @Override
    public void readFromNBT(NBTTagCompound compound)
    {
        super.readFromNBT(compound);
        
        chargeLevel = compound.getInteger("chargeLevel");
        lastMaxFuelLevel = compound.getInteger("lastMaxFuelLevel");
        currentFuelLevel = compound.getInteger("currentFuelLevel");
        
        NBTTagList items = compound.getTagList("Items");
        for (int i = 0; items.tagCount() > i; i++)
        {
            NBTTagCompound item = (NBTTagCompound) items.tagAt(i);
            int slot = item.getByte("Slot");
            
            if (slot >= 0 && slot < getSizeInventory())
            {
                setInventorySlotContents(slot, ItemStack.loadItemStackFromNBT(item));
            }
        }
    }
    
    @Override
    public void writeToNBT(NBTTagCompound compound)
    {
        super.writeToNBT(compound);
        
        compound.setInteger("chargeLevel", chargeLevel);
        compound.setInteger("lastMaxFuelLevel", lastMaxFuelLevel);
        compound.setInteger("currentFuelLevel", currentFuelLevel);
        
        NBTTagList items = new NBTTagList();
        for (int i = 0; i < getSizeInventory(); i++)
        {
            ItemStack stack = getStackInSlot(i);
            
            if (stack != null)
            {
                NBTTagCompound item = new NBTTagCompound();
                item.setByte("Slot", (byte) i);
                stack.writeToNBT(item);
                items.appendTag(item);
            }
        }
        compound.setTag("Items", items);
    }
    
    @Override
    public int getSizeInventory()
    {
        return 1;
    }
    
    @Override
    public ItemStack getStackInSlot(int slotID)
    {
        return fuelStack;
    }
    
    @Override
    public ItemStack decrStackSize(int slotID, int amount)
    {
        ItemStack stack = getStackInSlot(slotID);
        
        if (stack != null)
        {
            ItemStack temp;
            temp = stack.copy();
            
            if (amount >= stack.stackSize)
            {
                setInventorySlotContents(slotID, null);
                
                temp.stackSize = stack.stackSize;
            }
            else
            {
                setInventorySlotContents(slotID, new ItemStack(stack.itemID, stack.stackSize - amount, stack.getItemDamage()));
                
                temp.stackSize = amount;
            }
            
            return temp;
        }
        return null;
    }
    
    @Override
    public ItemStack getStackInSlotOnClosing(int slotID)
    {
        ItemStack returnStack = getStackInSlot(slotID);
        
        setInventorySlotContents(slotID, null);
        
        return returnStack;
    }
    
    @Override
    public void setInventorySlotContents(int slotID, ItemStack stack)
    {
        if (stack != null && stack.stackSize > getInventoryStackLimit())
        {
            stack.stackSize = getInventoryStackLimit();
        }
        
        fuelStack = stack;
        
        onInventoryChanged();
    }
    
    @Override
    public String getInvName()
    {
        return "Electric Generator";
    }
    
    @Override
    public boolean isInvNameLocalized()
    {
        return false;
    }
    
    @Override
    public int getInventoryStackLimit()
    {
        return 64;
    }
    
    @Override
    public boolean isUseableByPlayer(EntityPlayer entityplayer)
    {
        return entityplayer.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) <= 64;
    }
    
    @Override
    public void openChest()
    {}
    
    @Override
    public void closeChest()
    {}
    
    @Override
    public boolean isItemValidForSlot(int slotID, ItemStack stack)
    {
        return TileEntityFurnace.getItemBurnTime(stack) > 0;
    }
}*/
