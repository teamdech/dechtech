package dechmods.dechtech.tileentities;

import dechmods.dechtech.util.Util;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public abstract class TileEntityElectricInvBase extends TileEntityElectricBase implements ISidedInventory
{
    @Override
    public boolean isInvNameLocalized()
    {
        return false;
    }

    @Override
    public int getInventoryStackLimit()
    {
        return 64;
    }

    @Override
    public boolean isUseableByPlayer(EntityPlayer entityplayer)
    {
        return entityplayer.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) <= 64;
    }

    @Override
    public void openChest() { }

    @Override
    public void closeChest() { }
    
    @Override
    public ItemStack decrStackSize(int slotID, int amount)
    {
        ItemStack stack = getStackInSlot(slotID);
        
        if (stack != null)
        {
            ItemStack temp;
            temp = stack.copy();
            
            if (amount >= stack.stackSize)
            {
                setInventorySlotContents(slotID, null);
                
                temp.stackSize = stack.stackSize;
            }
            else
            {
                setInventorySlotContents(slotID, new ItemStack(stack.itemID, stack.stackSize - amount, stack.getItemDamage()));
                
                temp.stackSize = amount;
            }
            
            return temp;
        }
        return null;
    }
    
    @Override
    public ItemStack getStackInSlotOnClosing(int slotID)
    {
        ItemStack returnStack = getStackInSlot(slotID);
        
        setInventorySlotContents(slotID, null);
        
        return returnStack;
    }
    
    @Override
    public void writeToNBT(NBTTagCompound compound)
    {
        super.writeToNBT(compound);
        Util.saveInventory(compound, this);
    }
    
    @Override
    public void readFromNBT(NBTTagCompound compound)
    {
        super.readFromNBT(compound);
        Util.loadInventory(compound, this);
    }
}
