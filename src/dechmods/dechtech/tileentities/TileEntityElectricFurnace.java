package dechmods.dechtech.tileentities;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityFurnace;

public class TileEntityElectricFurnace extends TileEntity implements IInventory
{
    public ItemStack inputStack = null, outputStack = null;
    public int charge;
    public int progress;
    public boolean mk2;
    
    @Override
    public void updateEntity()
    {
        //TODO code!
    }
    
    @Override
    public void readFromNBT(NBTTagCompound compound)
    {
        super.readFromNBT(compound);
        
        NBTTagList items = compound.getTagList("Items");
        for (int i = 0; items.tagCount() > i; i++)
        {
            NBTTagCompound item = (NBTTagCompound) items.tagAt(i);
            int slot = item.getByte("Slot");
            
            if (slot >= 0 && slot < getSizeInventory())
            {
                setInventorySlotContents(slot, ItemStack.loadItemStackFromNBT(item));
            }
        }
    }
    
    @Override
    public void writeToNBT(NBTTagCompound compound)
    {
        super.writeToNBT(compound);
        
        NBTTagList items = new NBTTagList();
        for (int i = 0; i < getSizeInventory(); i++)
        {
            ItemStack stack = getStackInSlot(i);
            
            if (stack != null)
            {
                NBTTagCompound item = new NBTTagCompound();
                item.setByte("Slot", (byte) i);
                stack.writeToNBT(item);
                items.appendTag(item);
            }
        }
        compound.setTag("Items", items);
    }
    
    @Override
    public int getSizeInventory()
    {
        return 2;
    }

    @Override
    public ItemStack getStackInSlot(int slotID)
    {
        return slotID == 0 ? inputStack : outputStack;
    }

    @Override
    public ItemStack decrStackSize(int slotID, int amount)
    {
        ItemStack stack = getStackInSlot(slotID);
        
        if (stack != null)
        {
            ItemStack temp;
            temp = stack.copy();
            
            if (amount >= stack.stackSize)
            {
                setInventorySlotContents(slotID, null);
                
                temp.stackSize = stack.stackSize;
            }
            else
            {
                setInventorySlotContents(slotID, new ItemStack(stack.itemID, stack.stackSize - amount, stack.getItemDamage()));
                
                temp.stackSize = amount;
            }
            
            return temp;
        }
        return null;
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int slotID)
    {
        ItemStack returnStack = getStackInSlot(slotID);
        
        setInventorySlotContents(slotID, null);
        
        return returnStack;
    }

    @Override
    public void setInventorySlotContents(int slotID, ItemStack stack)
    {
        if (stack != null && stack.stackSize > getInventoryStackLimit()) stack.stackSize = getInventoryStackLimit();
        
        if (slotID == 0) inputStack = stack;
        if (slotID == 1) outputStack = stack;
        
        onInventoryChanged();
    }

    @Override
    public String getInvName()
    {
        return "Electric Furnace";
    }

    @Override
    public boolean isInvNameLocalized()
    {
        return false;
    }

    @Override
    public int getInventoryStackLimit()
    {
        return 64;
    }

    @Override
    public boolean isUseableByPlayer(EntityPlayer entityplayer)
    {
        return entityplayer.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) <= 64;
    }

    @Override
    public void openChest() { }

    @Override
    public void closeChest() { }

    @Override
    public boolean isItemValidForSlot(int slotID, ItemStack stack)
    {
        return slotID == 0 && FurnaceRecipes.smelting().getSmeltingResult(stack) != null;
    }
}
