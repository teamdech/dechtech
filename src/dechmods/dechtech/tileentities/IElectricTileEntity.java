package dechmods.dechtech.tileentities;

public interface IElectricTileEntity
{
    public String getMachineType();
}