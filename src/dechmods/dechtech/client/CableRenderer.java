package dechmods.dechtech.client;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.world.IBlockAccess;
import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import dechmods.dechtech.DeChTech;
import dechmods.dechtech.blocks.BlockCable;
import dechmods.dechtech.util.Util;

public class CableRenderer implements ISimpleBlockRenderingHandler
{
    public static int renderID;
    public static final float pixel = 1F / 16F;
    
    public static final int[] maxUMap = new int[] { 4, 8, 12, 12, 16, 8, 16, 12, 4, 8, 12, 8, 4, 4, 16, 16};
    public static final int[] maxVMap = new int[] { 4, 4, 4, 12, 4, 8, 8, 16, 8, 12, 8, 16, 12, 16, 12, 16};
    
    float minU, maxU, minV, maxV;
    
    public CableRenderer(int renderID)
    {
        CableRenderer.renderID = renderID;
    }
    
    @Override
    public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer)
    {}
    
    @Override
    public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer)
    {
        boolean[] canConnect = new boolean[6];
        
        canConnect[0] = Util.isCableBlock(world, x, y - 1, z);
        canConnect[1] = Util.isCableBlock(world, x, y + 1, z);
        canConnect[2] = Util.isCableBlock(world, x, y, z - 1);
        canConnect[3] = Util.isCableBlock(world, x, y, z + 1);
        canConnect[4] = Util.isCableBlock(world, x - 1, y, z);
        canConnect[5] = Util.isCableBlock(world, x + 1, y, z);
        
        Tessellator t = Tessellator.instance;
        
        t.setBrightness(DeChTech.blockCable.getMixedBrightnessForBlock(world, x, y, z));
        t.setColorOpaque_F(1F, 1F, 1F);
        
        if (!canConnect[0])
        {
            bindCrossing(canConnect[2], canConnect[5], canConnect[3], canConnect[4]);
            
            t.addVertexWithUV(x + pixel * 6, y + pixel * 6, z + pixel * 6, minU, minV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 6, z + pixel * 6, maxU, minV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 6, z + pixel * 10, maxU, maxV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 6, z + pixel * 10, minU, maxV);
        }
        else
        {
            minU = BlockCable.cable.getInterpolatedU(6);
            minV = BlockCable.cable.getInterpolatedV(10);
            maxU = BlockCable.cable.getInterpolatedU(10);
            maxV = BlockCable.cable.getInterpolatedV(16);
            
            t.addVertexWithUV(x + pixel * 6, y + pixel * 6, z + pixel * 6, maxU, minV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 6, z + pixel * 6, minU, minV);
            t.addVertexWithUV(x + pixel * 10, y, z + pixel * 6, minU, maxV);
            t.addVertexWithUV(x + pixel * 6, y, z + pixel * 6, maxU, maxV);
            
            t.addVertexWithUV(x + pixel * 10, y + pixel * 6, z + pixel * 10, maxU, minV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 6, z + pixel * 10, minU, minV);
            t.addVertexWithUV(x + pixel * 6, y, z + pixel * 10, minU, maxV);
            t.addVertexWithUV(x + pixel * 10, y, z + pixel * 10, maxU, maxV);

            t.addVertexWithUV(x + pixel * 6, y + pixel * 6, z + pixel * 10, maxU, minV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 6, z + pixel * 6, minU, minV);
            t.addVertexWithUV(x + pixel * 6, y, z + pixel * 6, minU, maxV);
            t.addVertexWithUV(x + pixel * 6, y, z + pixel * 10, maxU, maxV);
            
            t.addVertexWithUV(x + pixel * 10, y + pixel * 6, z + pixel * 6, maxU, minV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 6, z + pixel * 10, minU, minV);
            t.addVertexWithUV(x + pixel * 10, y, z + pixel * 10, minU, maxV);
            t.addVertexWithUV(x + pixel * 10, y, z + pixel * 6, maxU, maxV);
        }
        
        if (!canConnect[1])
        {
            bindCrossing(canConnect[2], canConnect[5], canConnect[3], canConnect[4]);
            
            t.addVertexWithUV(x + pixel * 6, y + pixel * 10, z + pixel * 6, minU, minV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 10, z + pixel * 10, minU, maxV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 10, z + pixel * 10, maxU, maxV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 10, z + pixel * 6, maxU, minV);
        }
        else
        {
            minU = BlockCable.cable.getInterpolatedU(6);
            minV = BlockCable.cable.getInterpolatedV(10);
            maxU = BlockCable.cable.getInterpolatedU(10);
            maxV = BlockCable.cable.getInterpolatedV(16);
            
            t.addVertexWithUV(x + pixel * 6, y + 1, z + pixel * 6, maxU, minV);
            t.addVertexWithUV(x + pixel * 10, y + 1, z + pixel * 6, minU, minV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 10, z + pixel * 6, minU, maxV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 10, z + pixel * 6, maxU, maxV);
            
            t.addVertexWithUV(x + pixel * 10, y + 1, z + pixel * 10, maxU, minV);
            t.addVertexWithUV(x + pixel * 6, y + 1, z + pixel * 10, minU, minV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 10, z + pixel * 10, minU, maxV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 10, z + pixel * 10, maxU, maxV);

            t.addVertexWithUV(x + pixel * 6, y + 1, z + pixel * 10, maxU, minV);
            t.addVertexWithUV(x + pixel * 6, y + 1, z + pixel * 6, minU, minV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 10, z + pixel * 6, minU, maxV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 10, z + pixel * 10, maxU, maxV);
            
            t.addVertexWithUV(x + pixel * 10, y + 1, z + pixel * 6, maxU, minV);
            t.addVertexWithUV(x + pixel * 10, y + 1, z + pixel * 10, minU, minV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 10, z + pixel * 10, minU, maxV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 10, z + pixel * 6, maxU, maxV);
        }
        
        if (!canConnect[2])
        {
            bindCrossing(canConnect[1], canConnect[4], canConnect[0], canConnect[5]);
            
            t.addVertexWithUV(x + pixel * 6, y + pixel * 10, z + pixel * 6, maxU, minV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 10, z + pixel * 6, minU, minV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 6, z + pixel * 6, minU, maxV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 6, z + pixel * 6, maxU, maxV);
        }
        else
        {
            minU = BlockCable.cable.getInterpolatedU(10);
            minV = BlockCable.cable.getInterpolatedV(6);
            maxU = BlockCable.cable.getInterpolatedU(16);
            maxV = BlockCable.cable.getInterpolatedV(10);
            
            t.addVertexWithUV(x + pixel * 6, y + pixel * 6, z, minU, maxV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 6, z, minU, minV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 6, z + pixel * 6, maxU, minV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 6, z + pixel * 6, maxU, maxV);
            
            t.addVertexWithUV(x + pixel * 6, y + pixel * 10, z, maxU, minV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 10, z + pixel * 6, minU, minV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 10, z + pixel * 6, minU, maxV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 10, z, maxU, maxV);
            
            t.addVertexWithUV(x + pixel * 6, y + pixel * 10, z + pixel * 6, maxU, minV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 10, z, minU, minV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 6, z, minU, maxV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 6, z + pixel * 6, maxU, maxV);
            
            t.addVertexWithUV(x + pixel * 10, y + pixel * 10, z, maxU, minV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 10, z + pixel * 6, minU, minV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 6, z + pixel * 6, minU, maxV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 6, z, maxU, maxV);
        }
        
        if (!canConnect[3])
        {
            bindCrossing(canConnect[1], canConnect[5], canConnect[0], canConnect[4]);
            
            t.addVertexWithUV(x + pixel * 10, y + pixel * 10, z + pixel * 10, maxU, minV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 10, z + pixel * 10, minU, minV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 6, z + pixel * 10, minU, maxV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 6, z + pixel * 10, maxU, maxV);
        }
        else
        {
            minU = BlockCable.cable.getInterpolatedU(0);
            minV = BlockCable.cable.getInterpolatedV(6);
            maxU = BlockCable.cable.getInterpolatedU(6);
            maxV = BlockCable.cable.getInterpolatedV(10);
            
            t.addVertexWithUV(x + pixel * 6, y + pixel * 6, z + pixel * 10, minU, maxV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 6, z + pixel * 10, minU, minV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 6, z + 1, maxU, minV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 6, z + 1, maxU, maxV);
            
            t.addVertexWithUV(x + pixel * 6, y + pixel * 10, z + pixel * 10, maxU, minV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 10, z + 1, minU, minV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 10, z + 1, minU, maxV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 10, z + pixel * 10, maxU, maxV);
            
            t.addVertexWithUV(x + pixel * 6, y + pixel * 10, z + 1, maxU, minV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 10, z + pixel * 10, minU, minV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 6, z + pixel * 10, minU, maxV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 6, z + 1, maxU, maxV);
            
            t.addVertexWithUV(x + pixel * 10, y + pixel * 10, z + pixel * 10, maxU, minV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 10, z + 1, minU, minV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 6, z + 1, minU, maxV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 6, z + pixel * 10, maxU, maxV);
        }
        
        if (!canConnect[4])
        {
            bindCrossing(canConnect[1], canConnect[3], canConnect[0], canConnect[2]);
            
            t.addVertexWithUV(x + pixel * 6, y + pixel * 10, z + pixel * 10, maxU, minV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 10, z + pixel * 6, minU, minV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 6, z + pixel * 6, minU, maxV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 6, z + pixel * 10, maxU, maxV);
        }
        else
        {
            minU = BlockCable.cable.getInterpolatedU(10);
            minV = BlockCable.cable.getInterpolatedV(6);
            maxU = BlockCable.cable.getInterpolatedU(16);
            maxV = BlockCable.cable.getInterpolatedV(10);
            
            t.addVertexWithUV(x, y + pixel * 6, z + pixel * 6, minU, minV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 6, z + pixel * 6, maxU, minV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 6, z + pixel * 10, maxU, maxV);
            t.addVertexWithUV(x, y + pixel * 6, z + pixel * 10, minU, maxV);
            
            t.addVertexWithUV(x, y + pixel * 10, z + pixel * 6, minU, minV);
            t.addVertexWithUV(x, y + pixel * 10, z + pixel * 10, minU, maxV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 10, z + pixel * 10, maxU, maxV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 10, z + pixel * 6, maxU, minV);
            
            t.addVertexWithUV(x, y + pixel * 10, z + pixel * 6, maxU, minV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 10, z + pixel * 6, minU, minV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 6, z + pixel * 6, minU, maxV);
            t.addVertexWithUV(x, y + pixel * 6, z + pixel * 6, maxU, maxV);
            
            t.addVertexWithUV(x + pixel * 6, y + pixel * 10, z + pixel * 10, maxU, minV);
            t.addVertexWithUV(x, y + pixel * 10, z + pixel * 10, minU, minV);
            t.addVertexWithUV(x, y + pixel * 6, z + pixel * 10, minU, maxV);
            t.addVertexWithUV(x + pixel * 6, y + pixel * 6, z + pixel * 10, maxU, maxV);
        }
        
        if (!canConnect[5])
        {
            bindCrossing(canConnect[1], canConnect[2], canConnect[0], canConnect[3]);
            
            t.addVertexWithUV(x + pixel * 10, y + pixel * 10, z + pixel * 6, maxU, minV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 10, z + pixel * 10, minU, minV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 6, z + pixel * 10, minU, maxV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 6, z + pixel * 6, maxU, maxV);
        }
        else
        {
            minU = BlockCable.cable.getInterpolatedU(0);
            minV = BlockCable.cable.getInterpolatedV(6);
            maxU = BlockCable.cable.getInterpolatedU(6);
            maxV = BlockCable.cable.getInterpolatedV(10);
            
            t.addVertexWithUV(x + pixel * 10, y + pixel * 6, z + pixel * 6, minU, minV);
            t.addVertexWithUV(x + 1, y + pixel * 6, z + pixel * 6, maxU, minV);
            t.addVertexWithUV(x + 1, y + pixel * 6, z + pixel * 10, maxU, maxV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 6, z + pixel * 10, minU, maxV);
            
            t.addVertexWithUV(x + pixel * 10, y + pixel * 10, z + pixel * 6, minU, minV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 10, z + pixel * 10, minU, maxV);
            t.addVertexWithUV(x + 1, y + pixel * 10, z + pixel * 10, maxU, maxV);
            t.addVertexWithUV(x + 1, y + pixel * 10, z + pixel * 6, maxU, minV);
            
            t.addVertexWithUV(x + pixel * 10, y + pixel * 10, z + pixel * 6, maxU, minV);
            t.addVertexWithUV(x + 1, y + pixel * 10, z + pixel * 6, minU, minV);
            t.addVertexWithUV(x + 1, y + pixel * 6, z + pixel * 6, minU, maxV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 6, z + pixel * 6, maxU, maxV);
            
            t.addVertexWithUV(x + 1, y + pixel * 10, z + pixel * 10, maxU, minV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 10, z + pixel * 10, minU, minV);
            t.addVertexWithUV(x + pixel * 10, y + pixel * 6, z + pixel * 10, minU, maxV);
            t.addVertexWithUV(x + 1, y + pixel * 6, z + pixel * 10, maxU, maxV);
        }
        
        return true;
    }
    
    public void bindCrossing(boolean b1, boolean b2, boolean b3, boolean b4)
    {
        int i = (b1 ? 1 : 0) + (b2 ? 2 : 0) + (b3 ? 4 : 0) + (b4 ? 8 : 0);
        
        if (i >= 0 && i < 16)
        {
            minU = BlockCable.crossings.getInterpolatedU(maxUMap[i] - 4);
            minV = BlockCable.crossings.getInterpolatedV(maxVMap[i] - 4);
            maxU = BlockCable.crossings.getInterpolatedU(maxUMap[i]);
            maxV = BlockCable.crossings.getInterpolatedV(maxVMap[i]);
        }
    }
    
    @Override
    public boolean shouldRender3DInInventory()
    {
        return false;
    }
    
    @Override
    public int getRenderId()
    {
        return renderID;
    }
}
