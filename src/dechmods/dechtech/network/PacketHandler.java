package dechmods.dechtech.network;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;


import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;
import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.common.network.Player;
import dechmods.dechtech.gui.ContainerGenerator;

public class PacketHandler implements IPacketHandler
{
    @Override
    public void onPacketData(INetworkManager manager, Packet250CustomPayload packet, Player player)
    {
        if (packet.channel.equals("packetdechtech"))
        {
            DataInputStream inputStream = new DataInputStream(new ByteArrayInputStream(packet.data));
            
            short channel, subchannel;

            try
            {
                channel = inputStream.readShort();
                subchannel = inputStream.readShort();
                
                if (channel == 0)
                {
                    if (subchannel == 0 && Minecraft.getMinecraft().thePlayer.openContainer instanceof ContainerGenerator)
                    {
                        ((ContainerGenerator)Minecraft.getMinecraft().thePlayer.openContainer).updateProgressBar(0, inputStream.readInt());
                    }
                }
            }
            catch (Exception e) { }
        }
    }
    
    public static void sendPacketToClient(Player player, int channel, int subchannel, int data)
    {
        ByteArrayOutputStream outputBytes = new ByteArrayOutputStream();
        DataOutputStream outputStream = new DataOutputStream(outputBytes);
        
        try
        {
            outputStream.writeShort(channel);
            outputStream.writeShort(subchannel);
            outputStream.writeInt(data);
        }
        catch (Exception e) { }
        
        PacketDispatcher.sendPacketToPlayer(new Packet250CustomPayload("packetdechtech", outputBytes.toByteArray()), player);
    }
}
