package dechmods.dechtech.enet;

import dechmods.dechtech.blocks.IElectricBlock;
import net.minecraft.block.Block;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class EnergyNetworkComponent
{   
    int x, y, z;
    World world;
    TileEntity theTile;
    
    public EnergyNetworkComponent(int x, int y, int z, World world)
    {
        int id = world.getBlockId(x, y, z);
        if (id != 0 && Block.blocksList[id] != null && Block.blocksList[id] instanceof IElectricBlock)
        {
            theTile = world.getBlockTileEntity(x, y, z);
        }
    }
}
