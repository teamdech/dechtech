package dechmods.dechtech.blocks;

import java.util.List;


import cpw.mods.fml.common.network.FMLNetworkHandler;
import dechmods.dechtech.DeChTech;
import dechmods.dechtech.tileentities.TileEntityElectricBase;
import dechmods.dechtech.tileentities.TileEntityElectricFurnace;
import dechmods.dechtech.tileentities.TileEntityGenerator;
import dechmods.dechtech.util.Util;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockMachine extends Block implements IElectricBlock
{
    public String[] blockTypes = new String[] { "EMITTER", "ACCEPTOR" };
    public int[] rates = new int[] { 40, 20, 40 };
    public int[] cap = new int[] { 160000, 320000 };
    
    public BlockMachine(int blockID)
    {
        super(blockID, Material.iron);
        setCreativeTab(CreativeTabs.tabBlock);
    }
    
    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ)
    {
        if (!world.isRemote)
        {
            int meta = world.getBlockMetadata(x, y, z);
            if (meta == 0) FMLNetworkHandler.openGui(player, DeChTech.instance, 0, world, x, y, z);
            if (meta == 1) FMLNetworkHandler.openGui(player, DeChTech.instance, 1, world, x, y, z);
        }
        
        return true;
    }
    
    @Override
    public void onPostBlockPlaced(World world, int x, int y, int z, int side)
    {
        
    }

    @Override
    public boolean hasTileEntity(int metadata)
    {
        return true;
    }
    
    @Override
    public TileEntity createTileEntity(World world, int meta)
    {
        if (meta == 0) return new TileEntityGenerator();
        if (meta == 1) return new TileEntityElectricFurnace();
        return null;
    }
    
    @Override
    public void getSubBlocks(int blockID, CreativeTabs tab, List items)
    {
        items.add(new ItemStack(blockID, 1, 0));
        items.add(new ItemStack(blockID, 1, 1));
    }

    @Override
    public int getCharge(World world, int x, int y, int z)
    {
        return ((TileEntityElectricBase) world.getBlockTileEntity(x, y, z)).chargeLevel;
    }

    @Override
    public void setCharge(World world, int x, int y, int z, int value)
    {
        ((TileEntityElectricBase) world.getBlockTileEntity(x, y, z)).chargeLevel = value;
    }

    @Override
    public String getBlockType(World world, int x, int y, int z)
    {
        int meta = world.getBlockMetadata(x, y, z);
        return meta >= 0 && meta < 2 ? blockTypes[meta] : "";
    }

    @Override
    public int getMaxCapacity(World world, int x, int y, int z)
    {
        int meta = world.getBlockMetadata(x, y, z);
        return meta >= 0 && meta < 2 ? cap[meta] : 0;
    }

    @Override
    public int getMaxEnergyRate(World world, int x, int y, int z)
    {
        int meta = world.getBlockMetadata(x, y, z);
        return meta >= 0 && meta < 2 ? rates[meta] : 0;
    }

    @Override
    public int pullEnergy(World world, int x, int y, int z, int energyUnits)
    {
        return Util.basicPull(world, x, y, z, energyUnits);
    }

    @Override
    public int addEnergy(World world, int x, int y, int z, int energyUnits)
    {
        return Util.basicPush(world, x, y, z, energyUnits);
    }
}
