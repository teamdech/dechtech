package dechmods.dechtech.blocks;

import java.util.List;

import dechmods.dechtech.client.CableRenderer;
import dechmods.dechtech.util.Util;



import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockCable extends Block
{
    public static final float pixel = 1F / 16F;
    public static Icon cable, crossings;
    
    public BlockCable(int blockID)
    {
        super(blockID, Material.circuits);
        setBlockBounds(6 * pixel, 6 * pixel, 6 * pixel, 10 * pixel, 10 * pixel, 10 * pixel);
        setCreativeTab(CreativeTabs.tabBlock);
    }
    
    @Override
    public void registerIcons(IconRegister iconRegister)
    {
        cable = iconRegister.registerIcon("dechtech:cable");
        crossings = iconRegister.registerIcon("dechtech:crossings");
    }
    
    @Override
    public int getRenderType()
    {
        return CableRenderer.renderID;
    }
    
    @Override
    public boolean isOpaqueCube()
    {
        return false;
    }
    
    @Override
    public void addCollisionBoxesToList(World world, int x, int y, int z, AxisAlignedBB bb, List list, Entity entity)
    {
        super.addCollisionBoxesToList(world, x, y, z, bb, list, entity);
        
        boolean[] canConnect = new boolean[6];
        
        canConnect[0] = Util.isCableBlock(world, x, y - 1, z);
        canConnect[1] = Util.isCableBlock(world, x, y + 1, z);
        canConnect[2] = Util.isCableBlock(world, x, y, z - 1);
        canConnect[3] = Util.isCableBlock(world, x, y, z + 1);
        canConnect[4] = Util.isCableBlock(world, x - 1, y, z);
        canConnect[5] = Util.isCableBlock(world, x + 1, y, z);

        if (canConnect[0])
        {
            setBlockBounds(6 * pixel, 0, 6 * pixel, 10 * pixel, 6 * pixel, 10 * pixel);
            super.addCollisionBoxesToList(world, x, y, z, bb, list, entity);
        }
        if (canConnect[1])
        {
            setBlockBounds(6 * pixel, 10 * pixel, 6 * pixel, 10 * pixel, 1, 10 * pixel);
            super.addCollisionBoxesToList(world, x, y, z, bb, list, entity);
        }
        if (canConnect[2])
        {
            setBlockBounds(6 * pixel, 6 * pixel, 0, 10 * pixel, 10 * pixel, 6 * pixel);
            super.addCollisionBoxesToList(world, x, y, z, bb, list, entity);
        }
        if (canConnect[3])
        {
            setBlockBounds(6 * pixel, 6 * pixel, 10 * pixel, 10 * pixel, 10 * pixel, 1);
            super.addCollisionBoxesToList(world, x, y, z, bb, list, entity);
        }
        if (canConnect[4])
        {
            setBlockBounds(0, 6 * pixel, 6 * pixel, 6 * pixel, 10 * pixel, 10 * pixel);
            super.addCollisionBoxesToList(world, x, y, z, bb, list, entity);
        }
        if (canConnect[5])
        {
            setBlockBounds(10 * pixel, 6 * pixel, 6 * pixel, 1, 10 * pixel, 10 * pixel);
            super.addCollisionBoxesToList(world, x, y, z, bb, list, entity);
        }

        setBlockBounds(6 * pixel, 6 * pixel, 6 * pixel, 10 * pixel, 10 * pixel, 10 * pixel);
    }
    
    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int x, int y, int z) 
    {
        boolean[] canConnect = new boolean[6];
        
        canConnect[0] = Util.isCableBlock(world, x, y - 1, z);
        canConnect[1] = Util.isCableBlock(world, x, y + 1, z);
        canConnect[2] = Util.isCableBlock(world, x, y, z - 1);
        canConnect[3] = Util.isCableBlock(world, x, y, z + 1);
        canConnect[4] = Util.isCableBlock(world, x - 1, y, z);
        canConnect[5] = Util.isCableBlock(world, x + 1, y, z);
        
        float minX, minY, minZ;
        float maxX, maxY, maxZ;
        
        minX = minY = minZ = 6 * pixel;
        maxX = maxY = maxZ = 10 * pixel;

        if (canConnect[0])
            minY = 0F;
        if (canConnect[1])
            maxY = 1F;
        if (canConnect[2])
            minZ = 0F;
        if (canConnect[3])
            maxZ = 1F;
        if (canConnect[4])
            minX = 0F;
        if (canConnect[5])
            maxX = 1F;
        
        return AxisAlignedBB.getBoundingBox(x + minX, y + minY, z + minZ, x + maxX, y + maxY, z + maxZ);
    }
    
    @Override
    public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z)
    {
        boolean[] canConnect = new boolean[6];
        
        canConnect[0] = Util.isCableBlock(world, x, y - 1, z);
        canConnect[1] = Util.isCableBlock(world, x, y + 1, z);
        canConnect[2] = Util.isCableBlock(world, x, y, z - 1);
        canConnect[3] = Util.isCableBlock(world, x, y, z + 1);
        canConnect[4] = Util.isCableBlock(world, x - 1, y, z);
        canConnect[5] = Util.isCableBlock(world, x + 1, y, z);
        
        float minX, minY, minZ;
        float maxX, maxY, maxZ;
        
        minX = minY = minZ = 6 * pixel;
        maxX = maxY = maxZ = 10 * pixel;

        if (canConnect[0])
            minY = 0F;
        if (canConnect[1])
            maxY = 1F;
        if (canConnect[2])
            minZ = 0F;
        if (canConnect[3])
            maxZ = 1F;
        if (canConnect[4])
            minX = 0F;
        if (canConnect[5])
            maxX = 1F;
        
        setBlockBounds(minX, minY, minZ, maxX, maxY, maxZ);
    }
}
