package dechmods.dechtech.blocks;

import net.minecraft.world.World;

public interface IElectricBlock
{   
    public int getCharge(World world, int x, int y, int z);
    public void setCharge(World world, int x, int y, int z, int value);
    public String getBlockType(World world, int x, int y, int z);
    public int getMaxCapacity(World world, int x, int y, int z);
    public int getMaxEnergyRate(World world, int x, int y, int z);
    public int pullEnergy(World world, int x, int y, int z, int energyUnits);
    public int addEnergy(World world, int x, int y, int z, int energyUnits);
}
