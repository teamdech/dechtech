package dechmods.dechtech.blocks;

import cpw.mods.fml.common.network.FMLNetworkHandler;
import dechmods.dechtech.DeChTech;
import dechmods.dechtech.tileentities.TileEntityElectricBase;
import dechmods.dechtech.tileentities.TileEntityGenerator;
import dechmods.dechtech.util.Util;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

public class BlockGenerator extends Block
{
    public Icon[] textures = new Icon[6];

    public BlockGenerator(int blockID)
    {
        super(blockID, Material.iron);
        setCreativeTab(CreativeTabs.tabBlock);
    }
    
    @Override
    public void registerIcons(IconRegister iconRegister)
    {   
        for (int i = 0; i < 6; i++)
            textures [i] = iconRegister.registerIcon("dechtech:" + i);
    }
    
    @Override
    public Icon getIcon(int side, int meta)
    {
        return textures[side];
    }
}
