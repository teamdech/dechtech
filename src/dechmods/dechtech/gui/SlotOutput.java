package dechmods.dechtech.gui;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotOutput extends Slot
{   
    public SlotOutput(IInventory tile, int slotID, int x, int y)
    {
        super(tile, slotID, x, y);
    }
    
    @Override
    public boolean isItemValid(ItemStack item)
    {
        return false;
    }
}
