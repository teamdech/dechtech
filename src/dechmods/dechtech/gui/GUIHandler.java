package dechmods.dechtech.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import cpw.mods.fml.common.network.IGuiHandler;
import dechmods.dechtech.tileentities.TileEntityElectricFurnace;
import dechmods.dechtech.tileentities.TileEntityGenerator;

public class GUIHandler implements IGuiHandler
{
    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
    {
        switch (ID)
        {
            case 0: return new ContainerGenerator((TileEntityGenerator) world.getBlockTileEntity(x, y, z), player.inventory);
            case 1: return new ContainerFurnace((TileEntityElectricFurnace) world.getBlockTileEntity(x, y, z), player.inventory);
        }
        return null;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
    {
        switch (ID)
        {
            case 0: return new GUIGenerator((ContainerGenerator)getServerGuiElement(ID, player, world, x, y, z));
            case 1: return new GUIFurnace((ContainerFurnace)getServerGuiElement(ID, player, world, x, y, z));
        }
        return null;
    }
}
