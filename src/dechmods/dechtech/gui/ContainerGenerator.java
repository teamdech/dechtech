package dechmods.dechtech.gui;

import cpw.mods.fml.common.network.Player;
import dechmods.dechtech.network.PacketHandler;
import dechmods.dechtech.tileentities.TileEntityGenerator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerGenerator extends Container
{
    public TileEntityGenerator tile;
    
    public int lastSentChargeLevel, lastSentFuelLevel, lastSentLastFuelLevel;

    public ContainerGenerator(TileEntityGenerator tile, InventoryPlayer invPlayer)
    {
        this.tile = tile;
        
        for (int x = 0; x < 9; x++)
        {
            addSlotToContainer(new Slot(invPlayer, x, 8 + 18 * x, 142));
        }
        
        for (int y = 0; y < 3; y++)
        {
            for (int x = 0; x < 9; x++)
            {
                addSlotToContainer(new Slot(invPlayer, x + y * 9 + 9, 8 + 18 * x, 84 + y * 18));
            }
        }
        
        this.addSlotToContainer(new SlotFuel(tile, 0, 80, 47));
    }
    
    @Override
    public boolean canInteractWith(EntityPlayer entityplayer)
    {
        return tile.isUseableByPlayer(entityplayer);
    }   
    
    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slotID)
    {
        return null;
    }
    
    @Override
    public void addCraftingToCrafters(ICrafting player)
    {
        super.addCraftingToCrafters(player);

        PacketHandler.sendPacketToClient((Player)player, 0, 0, tile.chargeLevel);
        
        player.sendProgressBarUpdate(this, 1, tile.currentFuelLevel);
        player.sendProgressBarUpdate(this, 2, tile.lastMaxFuelLevel);
    }
    
    @Override
    public void detectAndSendChanges()
    {
        super.detectAndSendChanges();
        
        if (tile.chargeLevel != lastSentChargeLevel)
        {
            for (Object player : crafters) PacketHandler.sendPacketToClient((Player) player, 0, 0, tile.chargeLevel);
            lastSentChargeLevel = tile.chargeLevel;
        }
        if (tile.currentFuelLevel != lastSentFuelLevel)
        {
            for (Object player : crafters) ((ICrafting) player).sendProgressBarUpdate(this, 1, tile.currentFuelLevel);
            lastSentFuelLevel = tile.currentFuelLevel;
        }
        if (tile.lastMaxFuelLevel != lastSentLastFuelLevel)
        {
            for (Object player : crafters) ((ICrafting) player).sendProgressBarUpdate(this, 2, tile.lastMaxFuelLevel);
            lastSentLastFuelLevel = tile.lastMaxFuelLevel;
        }
    }
    
    @Override
    public void updateProgressBar(int channel, int data) 
    {
        if (channel == 0) tile.chargeLevel = data;
        if (channel == 1) tile.currentFuelLevel = data;
        if (channel == 2) tile.lastMaxFuelLevel = data;
    }
}
