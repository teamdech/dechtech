package dechmods.dechtech.gui;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.ResourceLocation;

public class GUIFurnace extends GuiContainer
{   
    public ContainerFurnace furnace;
    
    public GUIFurnace(ContainerFurnace furnace)
    {
        super(furnace);
        
        this.furnace = furnace;
        
        xSize = 176;
        ySize = 166;
    }

    @Override
    public void drawGuiContainerBackgroundLayer(float f, int i, int j)
    {
        GL11.glColor4f(1, 1, 1, 1);
        
        Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("dechtech", "textures/gui/furnace.png"));
        drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
        
        int height = (int)((furnace.tile.charge / 80000F) * 64F);
        
        if (height > 0)
        {
            drawTexturedModalRect(guiLeft + 156, guiTop + 78 - height, 176, 14, 8, height);
            drawTexturedModalRect(guiLeft + 156, guiTop + 77, 176, 77, 8, 1);
        }
        
        if (furnace.tile.progress > 0)
        {
            drawTexturedModalRect(guiLeft + 45, guiTop + 53, 176, 0, 14, 14);
            drawTexturedModalRect(guiLeft + 70, guiTop + 34, 176, 78, (int)((furnace.tile.progress / 200F) * 22F), 16);
        }
    }
    
    @Override
    public void drawGuiContainerForegroundLayer(int x, int y) 
    {
        fontRenderer.drawString("Electric Furnace" + (furnace.tile.mk2 ? " MK2" : ""), 8, 6, 0x404040);

        x -= guiLeft;
        y -= guiTop;
        
        if (x >= 156 && x < 164 && y >= 14 && y < 78)
        {
            List<String> text = new ArrayList<String>();
            text.add(furnace.tile.charge + " MCEU");
            text.add("\u00a78Max 80000 MCEU");
            
            drawHoveringText(text, x, y, fontRenderer);
        }
    }
}
