package dechmods.dechtech.gui;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityFurnace;

public class SlotFuel extends Slot
{
    public SlotFuel(IInventory tile, int slotID, int x, int y)
    {
        super(tile, slotID, x, y);
    }
    
    @Override
    public boolean isItemValid(ItemStack item)
    {
        return TileEntityFurnace.isItemFuel(item);
    }
}
