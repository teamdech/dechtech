package dechmods.dechtech.gui;

import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;

public class TooltipHandler
{   
    @ForgeSubscribe
    public void getItemTooltip(ItemTooltipEvent event)
    {
        if (TileEntityFurnace.isItemFuel(event.itemStack))
        {
            event.toolTip.add("Can produce " + (TileEntityFurnace.getItemBurnTime(event.itemStack) * 10) + " MCEU (Electric Generator)");
        }
    }
}
